from sqlalchemy import Boolean, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from core.database import Base


class Class(Base):
    __tablename__ = "class"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    students = relationship("Student", backref="_class")


class Student(Base):
    __tablename__ = "student"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String)
    year = Column(String)
    examines = relationship("Examine", backref='_student')
    class_id = Column(Integer, ForeignKey('class.id'))
    is_active = Column(Boolean, default=True)


class Examine(Base):
    __tablename__ = "examine"

    id = Column(Integer, primary_key=True, index=True)
    student_id = Column(Integer, ForeignKey('student.id'))
    name = Column(String)
    date = Column(String)
    result = Column(Boolean)
    note = Column(String)
