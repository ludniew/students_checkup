from typing import Optional, List
from pydantic import BaseModel


class Examine(BaseModel):
    id: int
    name: str
    date: str
    result: bool
    note: str

    class Config:
        orm_mode = True


class CreateExamine(BaseModel):
    name: str
    result: bool
    student_id: int
    note: str = ""


class GetExaminesWithResults(BaseModel):
    name: str
    result: bool


class Student(BaseModel):
    id: int
    name: str
    surname: str
    year: str
    examines: List[Examine] = []
    is_active: bool

    class Config:
        orm_mode = True


class GetStudent(BaseModel):
    id: int


class CreateStudent(BaseModel):
    name: str
    surname: str
    year: str
    class_id: int


class DeleteStudentBySurname(BaseModel):
    surname: str


class UpdateStudent(BaseModel):
    name: Optional[str]
    surname: Optional[str]
    year: Optional[str]


class CreateClass(BaseModel):
    name: str


class DeleteClass(BaseModel):
    id: int


class UpdateClass(CreateClass):
    pass


class Class(UpdateClass):
    id: str
    name: str
    students: List[Student] = []

    class Config:
        orm_mode = True


class Summary(BaseModel):
    class_name: str
    number_of_students: int
    number_of_students_with_passed_exams: int
    examines_performed: int
