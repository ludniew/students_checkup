from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends

import domain.services.manage_service as manage_service

import core.database as database

router = APIRouter()


@router.get("/{class_id}/clean_students")
def clean_students_for_class(class_id, db: Session = Depends(database.get_db)):
    manage_service.clean_students_for_class(db, class_id=class_id)

    return {"status": "success"}


@router.get("/{student_id}/clean_exams")
def clean_exams_for_student(student_id, db: Session = Depends(database.get_db)):
    manage_service.clean_exams_for_student(db, student_id=student_id)

    return {"status": "success"}
