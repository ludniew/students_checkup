from typing import List

from sqlalchemy.orm import Session

import domain.services.classes_service as classes_service

import core.schemas.schemas as schemas
import core.database as database

from fastapi import APIRouter, Depends, HTTPException

router = APIRouter()


@router.get("/{class_id}", response_model=schemas.Class)
def get_class_by_id(class_id, db: Session = Depends(database.get_db)):
    try:
        db_class = classes_service.get_class_by_id(db, class_id=class_id)
        return db_class

    except Exception:
        raise HTTPException(status_code=500, detail=f"Critical server issue!")


@router.get("/", response_model=List[schemas.Class])
def get_classes(db: Session = Depends(database.get_db)):
    try:
        db_classes = classes_service.get_classes(db)
        return db_classes

    except Exception:
        raise HTTPException(status_code=500, detail=f"Critical server issue!")


@router.post("/", response_model=schemas.Class)
def create_class(create_class_data: schemas.CreateClass, db: Session = Depends(database.get_db)):
    try:
        return classes_service.create_class(db=db, payload=create_class_data)

    except Exception:
        raise HTTPException(status_code=500, detail=f"Critical server issue!")


@router.delete("/{class_id}")
def delete_class_by_id(class_id, db: Session = Depends(database.get_db)):
    try:
        classes_service.delete_class_by_id(db, class_id=class_id)
        return {"status": "success"}

    except Exception:
        raise HTTPException(status_code=500, detail=f"Critical server issue!")


@router.put("/{class_id}")
def update_class_data(class_id, update_class_data: schemas.UpdateClass, db: Session = Depends(database.get_db)):
    try:
        classes_service.update_class(db, class_id=class_id, payload=update_class_data)
        return {"status": "success"}

    except Exception:
        raise HTTPException(status_code=500, detail=f"Critical server issue!")
