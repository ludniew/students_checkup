from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends

import domain.services.examines_service as examines_service

import core.schemas.schemas as schemas
import core.database as database


router = APIRouter()


@router.post("/", response_model=schemas.Examine)
def create_class(create_examine_data: schemas.CreateExamine, db: Session = Depends(database.get_db)):
    return examines_service.create_examine(db=db, payload=create_examine_data)
