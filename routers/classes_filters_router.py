from typing import List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends

import domain.services.classes_service as classes_service

import core.schemas.schemas as schemas
import core.database as database


router = APIRouter()


@router.get("/{class_id}/students", response_model=List[schemas.Student])
def get_students_for_class(class_id, db: Session = Depends(database.get_db)):
    db_class = classes_service.get_class_by_id(db=db, class_id=class_id)

    return db_class.students


@router.get("/{class_id}/students/examines_passed", response_model=List[schemas.Student])
def get_students_with_passes_for_class(class_id, db: Session = Depends(database.get_db)):
    return classes_service.get_students_with_passes_for_class(db=db, class_id=class_id)


@router.get("/{class_id}/students/any_examines_passed", response_model=List[schemas.Student])
def get_students_with_any_passes_for_class(class_id, db: Session = Depends(database.get_db)):
    return classes_service.get_students_with_any_passes_for_class(db=db, class_id=class_id)


@router.get("/{class_id}/examines", response_model=List[schemas.GetExaminesWithResults])
def get_examines_performed_for_class(class_id, db: Session = Depends(database.get_db)):
    return classes_service.get_examines_performed_for_class(db=db, class_id=class_id)


@router.get("/{class_id}/summary", response_model=schemas.Summary)
def get_class_summary(class_id, db: Session = Depends(database.get_db)):
    return classes_service.get_class_summary(db=db, class_id=class_id)
