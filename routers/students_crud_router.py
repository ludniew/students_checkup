from typing import List

from sqlalchemy.orm import Session

import domain.services.students_service as students_service

import core.schemas.schemas as schemas
import core.database as database

from fastapi import APIRouter, Depends

router = APIRouter()


@router.get("/{student_id}", response_model=schemas.Student)
def get_student_by_id(student_id, db: Session = Depends(database.get_db)):
    db_student = students_service.get_student_by_id(db, student_id=student_id)

    return db_student


@router.get("/", response_model=List[schemas.Student])
def get_studentes(db: Session = Depends(database.get_db)):
    db_students = students_service.get_students(db)

    return db_students


@router.post("/", response_model=schemas.Student)
def create_student(create_student_data: schemas.CreateStudent, db: Session = Depends(database.get_db)):
    return students_service.create_student(db=db, payload=create_student_data)


@router.delete("/{student_id}")
def delete_student_by_id(student_id, db: Session = Depends(database.get_db)):
    students_service.delete_student_by_id(db, student_id=student_id)

    return {"status": "success"}


@router.delete("/by_surname/{surname}")
def delete_student_by_surname(surname, db: Session = Depends(database.get_db)):
    students_service.delete_student_by_surname(db, surname=surname)

    return {"status": "success"}


@router.put("/{student_id}")
def update_student_data(
        student_id, update_student_payload: schemas.UpdateStudent, db: Session = Depends(database.get_db)
):
    students_service.update_student(db, student_id=student_id, payload=update_student_payload)

    return {"status": "success"}
