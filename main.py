from fastapi import FastAPI

from core.database import engine, Base
from domain.services.databse_service import create_database

from routers import (
    classes_crud_router,
    students_crud_router,
    examines_crud_router,
    classes_filters_router,
    manage_router,
)

Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(
    classes_crud_router.router,
    prefix="/classes",
    tags=["classes"],
)
app.include_router(
    students_crud_router.router,
    prefix="/students",
    tags=["students"],
)
app.include_router(
    examines_crud_router.router,
    prefix="/examines",
    tags=["examines"],
)
app.include_router(
    classes_filters_router.router,
    prefix="/class_filter",
    tags=["class_filter"],
)
app.include_router(
    manage_router.router,
    prefix="/manage",
    tags=["manage"],
)

create_database()
