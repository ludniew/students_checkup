from fastapi import HTTPException
from sqlalchemy.orm import Session

import core.models.models as models
import core.schemas.schemas as schemas

from domain.services.classes_service import get_class_by_id


def get_student_by_id(db: Session, student_id: int):
    db_student = db.query(models.Student).filter(models.Student.id == student_id).first()

    if not db_student:
        raise HTTPException(status_code=404, detail=f"Student with id {student_id} does not exist!")

    return db_student


def get_student_by_surname(db: Session, surname: str):
    db_student = db.query(models.Student).filter(models.Student.surname == surname).first()

    if not db_student:
        raise HTTPException(status_code=404, detail=f"Student with surname {surname} does not exist!")

    return db_student


def get_students(db: Session):
    return db.query(models.Student).all()


def create_student(db: Session, payload: schemas.CreateStudent):
    db_class = get_class_by_id(db=db, class_id=payload.class_id)

    try:
        db_student = models.Student(
            name=payload.name,
            surname=payload.surname,
            year=payload.year,
            examines=[],
            _class=db_class
        )
        db.add(db_student)
        db.commit()
        db.refresh(db_student)
        return db_student

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def delete_student_by_id(db: Session, student_id: int):
    db_student = get_student_by_id(db=db, student_id=student_id)

    if not db_student:
        raise HTTPException(status_code=404, detail=f"student with id {student_id} does not exist!")

    try:
        db.query(models.Student).filter(models.Student.id == student_id).delete()
        db.commit()

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def delete_student_by_surname(db: Session, surname: str):
    db_student = get_student_by_surname(db=db, surname=surname)

    if not db_student:
        raise HTTPException(status_code=404, detail=f"Student with surname {surname} does not exist!")

    try:
        db.query(models.Student).filter(models.Student.surname == surname).delete()
        db.commit()

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def update_student(db: Session, student_id: int, payload: schemas.UpdateStudent):
    db_student = get_student_by_id(db=db, student_id=student_id)
    db_student.name = payload.name
    db_student.surname = payload.surname
    db_student.year = payload.year

    try:
        db.add(db_student)
        db.commit()

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")
