from sqlalchemy.orm import Session

from domain.services.classes_service import get_class_by_id
from domain.services.students_service import get_student_by_id, delete_student_by_id
from domain.services.examines_service import delete_exam_by_id


def clean_exams_for_student(db: Session, student_id: int):
    db_student = get_student_by_id(db=db, student_id=student_id)

    for exam in db_student.examines:
        delete_exam_by_id(db=db, exam_id=exam.id)


def clean_students_for_class(db: Session, class_id: int):
    db_class = get_class_by_id(db=db, class_id=class_id)

    for student in db_class.students:
        delete_student_by_id(db=db, student_id=student.id)
