from datetime import date

from fastapi import HTTPException
from sqlalchemy.orm import Session

import core.models.models as models
import core.schemas.schemas as schemas

from domain.services.students_service import get_student_by_id


def create_examine(db: Session, payload: schemas.CreateExamine):
    db_student = get_student_by_id(db=db, student_id=payload.student_id)

    try:
        db_examine = models.Examine(
            name=payload.name,
            result=payload.result,
            _student=db_student,
            date=str(date.today()),
            note=payload.note,
        )
        db.add(db_examine)
        db.commit()
        db.refresh(db_examine)
        return db_examine

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue!")


def delete_exam_by_id(db: Session, exam_id: int):
    db_exam = db.query(models.Examine).filter(models.Examine.id == exam_id).first()

    if not db_exam:
        raise HTTPException(status_code=404, detail=f"Examine with id: {exam_id} does not exist!")

    db.query(models.Examine).filter(models.Examine.id == exam_id).delete()
    db.commit()
