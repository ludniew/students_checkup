from fastapi import HTTPException
from sqlalchemy.orm import Session

import core.models.models as models
import core.schemas.schemas as schemas


def get_class_by_id(db: Session, class_id: int):
    db_class = db.query(models.Class).filter(models.Class.id == class_id).first()

    if not db_class:
        raise HTTPException(status_code=404, detail=f"Class with id {class_id} does not exist!")

    return db_class


def get_class_by_name(db: Session, name: str):
    db_class = db.query(models.Class).filter(models.Class.name == name).first()

    if not db_class:
        raise HTTPException(status_code=404, detail=f"Class with name {name} does not exist!")

    return db_class


def get_classes(db: Session):
    return db.query(models.Class).all()


def create_class(db: Session, payload: schemas.CreateClass):
    try:
        db_class = models.Class(
            name=payload.name,
            students=[]
        )
        db.add(db_class)
        db.commit()
        db.refresh(db_class)
        return db_class

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def delete_class_by_id(db: Session, class_id: int):
    db_class = get_class_by_id(db=db, class_id=class_id)

    if not db_class:
        raise HTTPException(status_code=404, detail=f"Class with id {class_id} does not exist!")

    try:
        db.query(models.Class).filter(models.Class.id == class_id).delete()
        db.commit()

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def update_class(db: Session, class_id: int, payload: schemas.UpdateClass):
    db_class = get_class_by_id(db=db, class_id=class_id)
    db_class.name = payload.name

    try:
        db.add(db_class)
        db.commit()

    except Exception:
        raise HTTPException(status_code=500, detail=f"Internal database issue")


def get_students_for_class(class_id, db: Session):
    db_class = get_class_by_id(db, class_id=class_id)

    return db_class.students


def get_students_with_passes_for_class(class_id, db: Session):
    db_class = get_class_by_id(db, class_id=class_id)

    return [student for student in db_class.students if all([examine.result for examine in student.examines])]


def get_students_with_any_passes_for_class(class_id, db: Session):
    db_class = get_class_by_id(db, class_id=class_id)

    return [student for student in db_class.students if any([examine.result for examine in student.examines])]


def get_examines_performed_for_class(class_id, db: Session):
    examines = []
    db_class = get_class_by_id(db, class_id=class_id)

    for student in db_class.students:
        for examine in student.examines:
            examines.append({"name": examine.name, "result": examine.result})

    return examines


def get_class_summary(class_id, db: Session):
    db_class = get_class_by_id(db, class_id=class_id)

    return {
        "class_name": db_class.name,
        "number_of_students": len(db_class.students),
        "number_of_students_with_passed_exams": len(get_students_with_passes_for_class(db=db, class_id=class_id)),
        "examines_performed": len(get_examines_performed_for_class(db=db, class_id=class_id))
    }
